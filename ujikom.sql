-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Mar 2019 pada 05.05
-- Versi Server: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ujikom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `deskripsi`
--

CREATE TABLE IF NOT EXISTS `deskripsi` (
  `id_deskripsi` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_deskripsi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `deskripsi`
--

INSERT INTO `deskripsi` (`id_deskripsi`, `deskripsi`) VALUES
(6, 'saya ilham maulana ganteng'),
(13, '<h1 style="margin-left:40px; text-align:center"><span style="font-family:Comic Sans MS,cursive"><span style="font-size:20px"><span style="color:#2980b9"><strong><span style="background-color:#ecf0f1">INVENTARIS&nbsp;</span></strong></span></span></span></h1>\r\n\r\n<p style="text-align:center"><span style="font-size:16px">&nbsp;</span><span style="font-family:Comic Sans MS,cursive"><span style="font-size:20px"> &nbsp;<span style="color:#2980b9"> &nbsp;adalah tentang peminjaman barang dan adanya barang datang&nbsp;</span></span></span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:Comic Sans MS,cursive"><span style="font-size:20px"><span style="color:#2980b9">atau tentang data-data barang yang ada disekolah<em><strong> </strong></em></span><span style="color:#c0392b"><em><strong><u>SMKN 1 CIOMAS</u> </strong></em></span><span style="color:#2980b9"><em>ini.</em></span></span></span></p>\r\n\r\n<p style="text-align:center"><span style="font-family:Comic Sans MS,cursive"><span style="font-size:20px"><span style="color:#2980b9"><em>misalkan kamu bisa meminjam barang-barang yang ada di sekolah kamu misalkan :</em></span></span></span></p>\r\n\r\n<p style="text-align:center"><span style="color:#2980b9; font-family:Comic Sans MS,cursive"><span style="font-size:20px"><em>meminjam laptop </em></span></span></p>\r\n\r\n<p style="text-align:center"><span style="color:#2980b9; font-family:Comic Sans MS,cursive"><span style="font-size:20px"><em>atau </em></span></span></p>\r\n\r\n<p style="text-align:center"><span style="color:#2980b9; font-family:Comic Sans MS,cursive"><span style="font-size:20px"><em>pun peralatan&nbsp;</em></span></span><span style="color:#2980b9; font-family:Comic Sans MS,cursive"><span style="font-size:20px"><em>lain</em></span></span></p>\r\n\r\n<p style="text-align:center"><span style="color:#2980b9; font-family:Comic Sans MS,cursive"><span style="font-size:20px"><em>nya tetapi kamu harus daftar dan isi data yang kamu pinjam diweb ini terlebih dahulu.</em></span></span></p>\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE IF NOT EXISTS `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE IF NOT EXISTS `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(23) NOT NULL,
  `kondisi` varchar(300) NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(24) NOT NULL,
  `id_jenis` int(30) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` int(45) NOT NULL,
  `id_petugas` int(23) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=124 ;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`) VALUES
(2, 'kursi', 'baik', '-', 0, 23, '2019-02-06', 21, 34, 897654),
(3, 'infocus', 'sangat baik', '-', 0, 214, '2019-02-06', 23, 24, 8976545),
(4, 'layar infocus', 'kurang baik', 'layar nya agar rusak dan buram', 3, 25, '2019-02-06', 23, 342, 9867575),
(5, 'laptop', 'baik', '-', 0, 27, '2019-02-06', 26, 32, 2147483647),
(6, 'mouse', 'baik', '-', 0, 31, '2019-02-06', 24, 32, 2345),
(7, 'charger', 'baik', '-', 0, 76, '2019-02-06', 52, 8, 97675757),
(123, 'RPL', 'baik', 'jg', 3, 3, '2019-02-06', 3, 123, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(22) NOT NULL,
  `kode_jenis` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'kayu', 32, 'baik'),
(2, 'besi', 24, 'sangat baik'),
(3, 'tembaga', 11, 'baik sekali');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(34) NOT NULL,
  `nama_level` varchar(23) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'administrator'),
(2, 'peminjaman'),
(3, 'operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(22) NOT NULL,
  `nip` varchar(17) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE IF NOT EXISTS `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_pinjaman` varchar(9) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_pinjaman`, `id_pegawai`) VALUES
(76575, '2019-03-09 00:00:00', '2019-03-09 00:00:00', 'pinjam', 757);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `id_petugas` int(23) NOT NULL,
  `username` varchar(23) NOT NULL,
  `password` varchar(21) NOT NULL,
  `nama_petugas` varchar(43) NOT NULL,
  `id_level` int(13) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `id_level`) VALUES
(1, 'OPERATOR', 'opr', 'operator21', 2),
(2, 'ADMINISTRATOR', 'adm', 'ilham', 1),
(4, 'PEMINJAMAN', 'pmj', 'maulana', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE IF NOT EXISTS `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(22) NOT NULL,
  `kode_ruang` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'lab rpl 2', 21, 'sangat baik'),
(2, 'lab rpl 1', 21, 'sangat baik'),
(3, 'studio animation', 23, 'kurang baik'),
(4, 'bengkel', 32, 'baik sekali'),
(5, 'kelas', 32, 'bagus');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
