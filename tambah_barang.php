<?php
include ('cek.php');
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Admin Inventory Sekolah</title>
        <!-- Bootstrap -->
         <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i>  Admin <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="profile.php">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="#">Dashboard</a>
                            </li>
                           
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="barang.php"><i class="icon-chevron-right"></i> Barang</a>
                        </li>
                        <li>
                            <a href="mutasi.php"><i class="icon-chevron-right"></i> Mutasi </a>
                        </li>
                        <li>
                            <a href="inventory.php"><i class="icon-chevron-right"></i> Inventory</a>
                        </li>
						<li>
                            <a href="view.php"><i class="icon-chevron-right"></i> View </a>
                        </li>
                        
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Di Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="#">Dashboard</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li>
	                                        <a href="#">Settings</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li class="active">Tools</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>

                    
					 <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Form Input</div>
								
                            </div>
							<br>
							
							
                            <div class="block-content collapse in">
                                <div class="span12">
                                  <form action=.php" method="post" class="form-horizontal">
                                      <fieldset>
                                       
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Kode Barang </label>
                                          <div class="controls">
                                            <input name="kd_barang" type="text"  placeholder="Masukan Kode Barang Anda" class="span6" id="typeahead"  data-provide="typeahead" 
											data-items="4">
                                            </div>
                                        </div>
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Merk </label>
                                          <div class="controls">
                                            <input name="merk" type="text"  placeholder="Masukan Merk Anda" class="span6" id="typeahead"  data-provide="typeahead" 
											data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Nama Barang </label>
                                          <div class="controls">
                                            <input name="nm_barang" type="text" placeholder="Masukan Nama Barang Anda" class="span6" id="typeahead"  data-provide="typeahead"
											data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Jumlah </label>
                                          <div class="controls">
                                            <input name="jml" type="text"  placeholder="Masukan Jumlah Anda" class="span6" id="typeahead"  data-provide="typeahead"
											data-items="4" >
                                            </div>
                                        </div>
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead"> Harga </label>
                                          <div class="controls">
                                            <input name="harga" type="text"  placeholder="Masukan Harga Anda" class="span6" id="typeahead"  data-provide="typeahead" 
											data-items="4" >
                                            </div>
                                        </div>
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Tahun Pembelian </label>
                                          <div class="controls">
                                            <input name="thn_pembelian" type="year"  placeholder="Masukan Tahun Pembelian Anda" class="span6" id="typeahead"  data-provide="typeahead" 
											data-items="4" >
                                            </div>
                                        </div>
										 <div class="control-group">
                                          <label class="control-label" for="typeahead">Nomer Seri Pabrik </label>
                                          <div class="controls">
                                            <input name="no_seri_pabrik" type="text"  placeholder="Masukan No Seri Pabrik Anda" class="span6" id="typeahead" 
											data-provide="typeahead" data-items="4">
                                            </div>
                                        </div>
										
										 <div class="control-group">
                                          <label class="control-label" for="typeahead">Bahan</label>
                                          <div class="controls">
                                            <input name="bahan" type="text"  placeholder="Masukan No Seri Pabrik Anda" class="span6" id="typeahead"  
											data-provide="typeahead" data-items="4">
                                            </div>
                                        </div>
										 <div class="control-group">
                                          <label class="control-label" for="typeahead">Bentuk</label>
                                          <div class="controls">
                                            <input name="bentuk" type="text"  placeholder="Masukan Bentuk Anda" class="span6" id="typeahead"  
											data-provide="typeahead" data-items="4">
                                            </div>
                                        </div>
										 <div class="control-group">
                                          <label class="control-label" for="typeahead">Qty</label>
                                          <div class="controls">
                                            <input name="b" type="number"  placeholder="Masukan Qty Anda" class="span6" id="typeahead"  
											data-provide="typeahead" data-items="4">
                                            </div>
                                        </div>
										 <div class="control-group">
                                          <label class="control-label" for="typeahead"></label>
                                          <div class="controls">
                                            <input name="kb" type="hidden"  placeholder="Masukan Qty Anda" class="span6" id="typeahead"  
											data-provide="typeahead" data-items="4">
                                            </div>
                                        </div>
										 <div class="control-group">
                                          <label class="control-label" for="typeahead"></label>
                                          <div class="controls">
                                            <input name="rb" type="hidden"  placeholder="Masukan Qty Anda" class="span6" id="typeahead"  
											data-provide="typeahead" data-items="4">
                                            </div>
                                        </div>
										<button type="submit" class="btn btn-success">Simpan</button>
										<button type="reset" class="btn btn-danger">Reset</button>
                                       
                                      </fieldset>
                                    </form>
                                </div>
							</div>
						</div>
						
						 <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Inventory Sekolah@ 2018</p>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>