
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>INVENTARIS</title>
        <!-- Bootstrap -->
         <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">inventaris</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i>  Admin <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="profile.php">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="layout.php">Dashboard</a>
                            </li>
                           
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                                     <div class="clearfix"></div>
                     <li><a href="Layout.php"><i class="fa fa-desktop fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i><span class="menu-title">dashboard</span></a>
                       
                    </li>
					 <li><a href="Forms.php"><i class="fa fa-edit fa-fw">
                        <div class="icon-bg bg-violet"></div>
                    </i><span class="menu-title">inventaris</span></a>
                      
                    </li>
                     <li><a href="Layout2.php"><i class="fa fa-desktop fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i><span class="menu-title">generate laporan</span></a>
                        <li><a href="Tables.php"><i class="fa fa-th-list fa-fw">
                        <div class="icon-bg bg-blue"></div>
                    </i><span class="menu-title"> peminjaman</span></a>
                          
                    </li>
                   
						  <li><a href="Tables2.php"><i class="fa fa-th-list fa-fw">
                        <div class="icon-bg bg-blue"></div>
                    </i><span class="menu-title">pengembalian</span></a>
                          
                    </li>
					  
						  <li><a href="lain_nya.php"><i class="fa fa-th-list fa-fw">
                        <div class="icon-bg bg-blue"></div>
                    </i><span class="menu-title">lain nya</span></a>
                          
                    </li>
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Di Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="#">Dashboard</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li>
	                                        <a href="#">Settings</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li class="active">Tools</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>

                    

                     <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Tabel Mutasi</div>
                            </div>
							<br>
							
							<a href="tambah_mutasi.php" class="btn btn-primary fa fa-plus" style="margin-left: 15px;">Tambah Barang</a>
									<br>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    
  									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
										<thead>
											<tr>
												
												
										    <th>id inventaris</th>
                                            <th>nama</th>
                                            <th>kondisi</th>
                                            <th>keterangan</th>
                                            <th>jumlah </th>
											<th>id jenis</th>
											<th>tanggal register</th>
											<th>id ruang</th>
											<th>kode inventaris </th>
											<th>id petugas</th>
											<th>option</th>
											
											</tr>
										</thead>
										<tbody>
											<?php
                                    include "koneksi.php";
                                    $no=1;
                                    $select=mysql_query("select * from mutasi1");
                                    while($data=mysql_fetch_array($select))
                                    {
                                    ?>
                                      <tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $data['id_inventaris']; ?></td>
										<td><?php echo $data['nama']; ?></td>
										<td><?php echo $data['kondisi']; ?></td>
										<td><?php echo $data['keterangan']; ?></td>
										<td><?php echo $data['jumlah']; ?></td>
										<td><?php echo $data['id_jenis']; ?></td>
										<td><?php echo $data['tanggal_register']; ?></td>
										<td><?php echo $data['id_ruang']; ?></td>
										<td><?php echo $data['kode_inventaris']; ?></td>
										<td><?php echo $data['id_petugas']; ?></td>
										<td><?php echo $data['option']; ?></td>
										
											<td><a class="btn btn outline btn-primary fa fa-edit" href="edit_mutasi.php?id=<?php echo $data['id']; ?>">Edit</a> 
											<a class="btn btn outline btn-danger fa fa-trash-o" href="hapus_mutasi.php?id=<?php echo $data['id']; ?>">Hapus</a></td>
	                                    
										 
											

                                        </tr>
										<?php
									}
									?>
											
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                     <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
           
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>