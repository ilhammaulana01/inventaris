<?php
include ('cek.php');
error_reporting(0);
session_start();
?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Inventaris</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
    #line-chart {
        height:300px;
        width:800px;
        margin: 0px auto;
        margin-top: 1em;
    }
    .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
        color: #fff;
    }
</style>

<script type="text/javascript">
    $(function() {
        var uls = $('.sidebar-nav > ul > *').clone();
        uls.addClass('visible-xs');
        $('#main-menu').append(uls.clone());
    });
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <!-- Le fav and touch icons -->
  <link rel="shortcut icon" href="../assets/ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
      <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
          <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
              <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
                  <!--[if (gt IE 9)|!(IE)]><!--> 
                  
                  <!--<![endif]-->

                  <div class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="" href="index.php"><span class="navbar-brand"><span class="fa fa-briefcase"></span> Inventaris Sarana dan Prasarana</span></a></div>

                    <div class="navbar-collapse collapse" style="height: 1px;">
                      <ul id="main-menu" class="nav navbar-nav navbar-right">
                        <li class="dropdown hidden-xs">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i> <?php echo $_SESSION['nama_petugas'];?> 
                                <i class="fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">


                                <li>
                                 <a href="logout.php">
                                  <i class="ace-icon fa fa-power-off"></i>
                                  Logout
                              </a>
                          </li>
                      </ul>
                  </li>
              </ul>

          </div>
      </div>
  </div>

  <div class="sidebar-nav">
     <?php

     if ($_SESSION['id_level']==1){

        echo'
        <ul>
        <li><a href="index.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>&nbsp Dashboard</a></li>
        <li>
        <li><a href="inventaris.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-briefcase"></i>&nbsp Inventaris</a></li>
        <li>
        <li><a href="peminjaman.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Peminjaman</a></li>
        <li>
        <li><a href="pengembalian.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class=" glyphicon glyphicon-arrow-left"></i>&nbsp Pengembalian</a></li>
        <li>
         <li><a href="#" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-paste"></i> Laporan<i class="fa fa-collapse"></i></a></li>
        <li><ul class="dashboard-menu nav nav-list collapse">
        <li ><a href="inventaris.php"><span class="fa fa-caret-right"></span> Laporan Inventaris</a></li>
        <li><a href="peminjaman.php"><span class="fa fa-caret-right"></span> Laporan Peminjaman</a></li>
        <li ><a href="pengembalian.php"><span class="fa fa-caret-right"></span> Laporan Pengembalian</a></li>
        </ul></li>
        
        <li>
        <li><a href="#" data-target=".accounts-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-list"></i> Lainnya<i class="fa fa-collapse"></i></a></li>
        <li><ul class="accounts-menu nav nav-list collapse">
        <li ><a href="jenis.php"><span class="fa fa-caret-right"></span> Jenis</a></li>
        <li><a href="ruang.php"><span class="fa fa-caret-right"></span> Ruang</a></li>
              <li ><a href="petugas.php"><span class="fa fa-caret-right"></span> Petugas</a></li>
        <li><a href="pegawai.php"><span class="fa fa-caret-right"></span> Pegawai</a></li>
        </ul></li></div>
        </ul></li>
        
        
        </ul></li>';
    }
    
    elseif ($_SESSION['id_level']==2){
     echo'
     <ul>
     <li><a href="index.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>&nbsp Dashboard</a></li>
     <li>
     <li><a href="peminjaman.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Peminjaman</a></li>
     <li>
     <li><a href="pengembalian.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Pengembalian</a></li>
     <li></ul>';
 }
 elseif ($_SESSION['id_level']==3){
     echo'<ul>
     <li><a href="index.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>&nbsp Dashboard</a></li>
     <li>
     <li><a href="peminjaman.php" data-target=".dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-plus"></i>&nbsp Peminjaman</a></li>
     <li></ul>';
 }
 ?>



</div>

 <div class="content">
        <div class="header">
            

            <h1 class="page-title">Peminjaman</h1>
                    <ul class="breadcrumb">
           
        </ul>

        </div>
        <div class="main-content">

    </div>  <div class="panel-body">
                             <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                            Form Kembali
                        </div>
                        <div class="panel-body">
                        <center><div class="panel-body">
                        <div class="col-lg-3 col-md-offset-4">
                <label>Pilih Id Peminjaman</label>
                <form method="POST">
                    <select name="id_peminjaman" class="form-control m-bot15">
                        <?php
                        include "koneksi.php";
                                //display values in combobox/dropdown
                        $result = mysql_query("SELECT id_peminjaman from peminjaman where status_peminjaman='Pinjam' ");
                        while($row = mysql_fetch_assoc($result))
                        {
                            echo "<option value='$row[id_peminjaman]'>$row[id_peminjaman]</option>";
                        } 
                        ?>
                   </select>
                                    <br/>
                                <button type="submit" name="pilih" class="btn btn-outline btn-primary">Tampilkan</button>
                            </form>
                        </div>
                    </div></center>

                <?php
                if(isset($_POST['pilih'])){?>
                  <form action="proses_pengembalian.php" method="post" enctype="multipart/form-data">
                     <?php
                     include "koneksi.php";
                     $id_peminjaman=$_POST['id_peminjaman'];
                     $select=mysql_query("select * from peminjaman i left join detail_pinjam p on p.id_detail_pinjam=i.id_peminjaman
                        where id_peminjaman='$id_peminjaman'");
                     while($data=mysql_fetch_array($select)){
                        ?>
                <div class="form-group">
                    <label>Id Inventaris</label>
                    <input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_inventaris" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="">
                </div><br>                    
               
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Tanggal Pinjam
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="datetime" name="" value="<?php echo $data['tanggal_pinjam']?>" class="form-control col-md-7 col-xs-12" disabled placeholder="tanggal_pinjam">
                        <input type="hidden" name="tanggal_pinjam" value="<?php echo $data['tanggal_pinjam']?>" >
                        <input type="hidden" name="tanggal_kembali" >
                    </div>
                </div><br><br><br>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        ID Pegawai
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="" value="<?php echo $data['id_pegawai']?>" class="form-control col-md-7 col-xs-12" disabled placeholder="ID Pegawai">
                        <input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai']?>" >
                    </div>
                </div><br><br><br>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Jumlah
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" name="jumlah" value="<?php echo $data['jumlah']?>" class="form-control col-md-7 col-xs-12"  placeholder="Jumlah">
                    </div>
                </div><br><br>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                     Status Peminjaman
                 </label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="status_peminjaman" class="form-control m-bot15">
                    <option><?php echo $data['status_peminjaman']?></option>
                    <option>Kembali</option>

                </select>
            </div><br><br><br>
            <button type="submit" class="btn">Simpan</button>
        </div>

    <?php } ?>
</form>

<?php } ?>


   <script type="text/javascript" src="assets/js/jquery.js"></script>
                                <script type="text/javascript" src="assets/js/jquery.min.js"></script>
                                <script type="text/javascript" src="assets/js/jquery.dataTables.js"></script>
                                <script type="text/javascript" src="assets/js/jquery.dataTables.min.js">
                                    
                                </script>
                            
                                <script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
                            </script>



            <footer>
                <hr>

                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p class="pull-right">Inventaris</a> 
                <p> © 2018 Sarana & Prasarana</a></p>
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
</body>

</html>
